//
//  City.swift
//  MambaTest
//
//  Created by Антон Семенец on 30/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class City: NSObject {

    var cityName: String
    var population: String
    
    init(cityName: String, population: String) {
        self.cityName = cityName
        self.population = population
    }
    
}
