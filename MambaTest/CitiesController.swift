//
//  CitiesController.swift
//  MambaTest
//
//  Created by Антон Семенец on 29/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class CitiesController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var navigation: UINavigationItem!
    @IBOutlet weak var cities: UITableView!
    
    private var citiesViewModel: CitiesViewModel!
    
    var activityIndicatorView: UIActivityIndicatorView!
    
    let cellIdentifier = "city"
    let dispatchQueue = DispatchQueue(label: "Dispatch Queue", attributes: [], target: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        cities.backgroundView = activityIndicatorView
        navigation.leftBarButtonItem = UIBarButtonItem(title: "Изменить", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CitiesController.editButtonPressed))
        navigation.leftBarButtonItem?.isEnabled = false
        
        navigation.rightBarButtonItem = UIBarButtonItem(title: "Добавить", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CitiesController.addButtonPressed))
        navigation.rightBarButtonItem?.isEnabled = false

        
        activityIndicatorView.startAnimating()
        
        dispatchQueue.async {
            Thread.sleep(forTimeInterval: 2)
            
            OperationQueue.main.addOperation() {
                self.activityIndicatorView.stopAnimating()
                self.navigation.leftBarButtonItem?.isEnabled = true
                self.navigation.rightBarButtonItem?.isEnabled = true
                self.citiesViewModel = CitiesViewModel()
                self.cities.reloadData()
                
            }
        }
    }
    
    
    func editButtonPressed(){
        cities.setEditing(!cities.isEditing, animated: true)
        if cities.isEditing == true{
            navigation.leftBarButtonItem = UIBarButtonItem(title: "Ок", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CitiesController.editButtonPressed))
        }else{
            navigation.leftBarButtonItem = UIBarButtonItem(title: "Изменить", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CitiesController.editButtonPressed))
        }
    }
    
    
    func addButtonPressed(){
        if cities.isEditing == true {
            cities.setEditing(!cities.isEditing, animated: true)
            navigation.leftBarButtonItem = UIBarButtonItem(title: "Изменить", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CitiesController.editButtonPressed))
        }

        self.performSegue(withIdentifier: "addCity", sender: nil);
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (citiesViewModel != nil ) {
            self.cities.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            citiesViewModel.deleteCityInformation(indexPath: indexPath as NSIndexPath)
            cities.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = (citiesViewModel != nil ) ? citiesViewModel.numberOfItems() :  0
        return count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CityViewCell
        cell?.city.text = citiesViewModel.cityNameForItemAtIndexPath(indexPath: indexPath as NSIndexPath)
        cell?.population.text = citiesViewModel.populationForItemAtIndexPath(indexPath: indexPath as NSIndexPath)
        return cell!
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "editCities", sender: indexPath);
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "editCities") {
            let nextScene = segue.destination as! CityEditorController
            let index = ( sender as! NSIndexPath)
            let currentCell = cities.cellForRow(at: index as IndexPath) as! CityViewCell
            nextScene.cityName = currentCell.city.text
            nextScene.population = currentCell.population.text
            nextScene.cityViewModel = citiesViewModel
            nextScene.item = index.row
            nextScene.mode = segue.identifier
        } else if (segue.identifier == "addCity") {
            let nextScene = segue.destination as! CityEditorController
            nextScene.mode = segue.identifier
            nextScene.cityViewModel = citiesViewModel
        }
    }

}
