//
//  CitiesViewModel.swift
//  MambaTest
//
//  Created by Антон Семенец on 30/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class CitiesViewModel: NSObject {
    
    private var sortedData = [City]()
    
    private var citiesClient = CitiesClient()
    
    
    override init() {
        self.sortedData = CitiesViewModel.sortCities(with: self.citiesClient.citiesData)
    }
    
    private static func sortCities(with citiesData: [City]) -> [City]  {
        let sortData = citiesData.sorted{
            Int($0.0.population.replacingOccurrences(of: " ", with: ""))! > Int($0.1.population.replacingOccurrences(of: " ", with: ""))!
        }
        return sortData
    }
    
    
    func numberOfItems() -> Int {
        return sortedData.count
    }
    
    
    func cityNameForItemAtIndexPath(indexPath: NSIndexPath) -> String {
        let cityName = ( sortedData.indices.contains(indexPath.row) ? sortedData[indexPath.row].cityName : "" )
        return cityName
    }
    
    
    func populationForItemAtIndexPath(indexPath: NSIndexPath) -> String {
        var population = ( sortedData.indices.contains(indexPath.row) ? sortedData[indexPath.row].population : "" )
        
        population = population.replacingOccurrences(of: " ", with: "")
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        if #available(iOS 9.0, *) {
            formatter.numberStyle = .decimal
        } else {
        }
        
        if ( population == "" ) {
            population = "0" ;
        }

        number = NSNumber(value: (Int(population)!))
        population = formatter.string(from: number)!
        population = population.replacingOccurrences(of: ",", with: " ")

        return population
    }
    
    
    func deleteCityInformation(indexPath: NSIndexPath) {
       _ = ( sortedData.indices.contains(indexPath.row) ? sortedData.remove(at: indexPath.row) : nil )
    }
    
    
    func addCity(cityName: String, population: String) {
        print(population)
        sortedData.append(City(cityName: cityName, population: population))
        sortedData = CitiesViewModel.sortCities(with: sortedData )
    }
    
    
    func updateCity(item: Int, cityName: String, population: String) {
        sortedData[item] = City (cityName: cityName, population: population)
        sortedData = CitiesViewModel.sortCities(with: sortedData )
    }
    
}
