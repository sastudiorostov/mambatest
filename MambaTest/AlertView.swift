//
//  AlertView.swift
//  MambaTest
//
//  Created by Антон Семенец on 30/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class AlertView: NSObject {
    class func setAlertView(delegateView: AnyObject, title:String, message: String) {
        delegateView.dismissKeyboard()
        
        let alert = UIAlertController(title: title, message: message, preferredStyle:UIAlertControllerStyle.alert) ;
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            delegateView.dismissKeyboard()
            
        })) ;
        delegateView.present(alert, animated: true, completion: { action in
        })
    }
}
