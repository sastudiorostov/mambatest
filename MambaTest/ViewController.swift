//
//  ViewController.swift
//  MambaTest
//
//  Created by Антон Семенец on 29/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var userViewModel = UserViewModel()

    @IBOutlet weak var fullName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateScreen()
    }

    
    func updateScreen() {
        fullName.text = userViewModel.fullName
    }
}

