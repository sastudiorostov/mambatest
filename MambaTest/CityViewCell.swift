//
//  CItyViewCell.swift
//  MambaTest
//
//  Created by Антон Семенец on 29/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class CityViewCell: UITableViewCell {

    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var population: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
