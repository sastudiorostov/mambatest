//
//  EditCityController.swift
//  MambaTest
//
//  Created by Антон Семенец on 30/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class CityEditorController: UIViewController {
    
    @IBOutlet weak var navigation: UINavigationItem!

    @IBOutlet weak var addEdit: UIButton!
    
    @IBOutlet weak var addEditOut: UIButton!
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var populationTextField: UITextField!
    
    @IBOutlet weak var sendView: UIView!
    
    
    var mode: String!
    
    var item: Int?
    var cityName: String?
    var population: String?
    
    var cityViewModel: CitiesViewModel?
    
    let dispatchQueue = DispatchQueue(label: "Dispatch Queue", attributes: [], target: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        navigation.leftBarButtonItem = UIBarButtonItem(title: "Отмена", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CityEditorController.cancelButtonPressed))

        if (mode == "editCities") {
            navigation.title = "Изменить"
            addEditOut.setTitle("Изменить", for: .normal)
            
            cityTextField.text = cityName
            populationTextField.text = population
        } else if (mode == "addCity") {
            
            navigation.title = "Новый город"
            addEditOut.setTitle("Новый город", for: .normal)
        }
    }
    
    
    func cancelButtonPressed(){
        self.performSegueToReturnBack()
    }

    
    @IBAction func addEditCity(_ sender: UIButton) {
        if (!validationFields()) { return }
        self.sendView.isHidden = false
        self.addEdit.isEnabled = false
        
        if (mode == "editCities") {
            
            dispatchQueue.async {
                Thread.sleep(forTimeInterval: 2)
                OperationQueue.main.addOperation() {

                    self.sendView.isHidden = true
                    
                    self.cityViewModel?.updateCity(item: self.item!, cityName: self.cityTextField.text!, population: self.populationTextField.text!)
                    
                    self.performSegueToReturnBack()
                    
                }
            }
            
        } else if (mode == "addCity") {
            
            dispatchQueue.async {
                Thread.sleep(forTimeInterval: 2)
                OperationQueue.main.addOperation() {
                    
                    self.sendView.isHidden = true
                    
                    self.cityViewModel?.addCity(cityName: self.cityTextField.text!, population: self.populationTextField.text!)
                    
                    self.performSegueToReturnBack()
                    
                }
            }
        }
    }
    
    
    private func validationFields() -> Bool {
        if ( cityTextField.text == "" ) {
            AlertView.setAlertView(delegateView: self, title: "Ошибка", message: "Вы не ввели название города")
            return false
        }
        if ( populationTextField.text == "" ) {
            AlertView.setAlertView(delegateView: self, title: "Ошибка", message: "Вы не ввели популяцию")
            return false
        }
        
        return true
    }
}
