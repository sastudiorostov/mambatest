//
//  UserClient.swift
//  MambaTest
//
//  Created by Антон Семенец on 30/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class UserClient: NSObject {
    
    var userData: User
    
    override init() {
        self.userData = User(surname: "Семенец", firstName: "Антон", middleName: "Юрьевич")
    }

}
