//
//  CitiesClient.swift
//  MambaTest
//
//  Created by Антон Семенец on 30/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class CitiesClient: NSObject {

    var citiesData = [City]()
    
    override init() {
        self.citiesData = [ City(cityName: "Ростов-на-Дону", population: "1 119 875"),
                            City(cityName: "Москва", population: "12 330 126"),
                            City(cityName: "Санкт-Петербург", population: "5 225 690"),
                            City(cityName: "Пермь", population: "1 041 876"),
                            City(cityName: "Нижний Новгороду", population: "1 266 871"),
                            City(cityName: "Челябинск", population: "1 191 994"),
                            City(cityName: "Волгоград", population: "1 016 137")]
    }
    
    
}
