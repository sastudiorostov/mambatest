//
//  UserViewModel.swift
//  MambaTest
//
//  Created by Антон Семенец on 30/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class UserViewModel: NSObject {

    var fullName: String
    
    private var userClient = UserClient()

    override init() {
        let user = self.userClient.userData
        fullName = UserViewModel.getFullName(with: user)
    }
    
    private static func getFullName(with user: User) -> String {
        let fullName = user.surname + " " + user.firstName + " " + user.middleName
        return fullName
    }

}
