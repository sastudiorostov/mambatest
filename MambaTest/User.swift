//
//  User.swift
//  MambaTest
//
//  Created by Антон Семенец on 30/07/2017.
//  Copyright © 2017 Anton Semenets. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var surname: String
    var firstName: String
    var middleName: String
    
    init(surname: String, firstName: String, middleName: String) {
        self.surname = surname
        self.firstName = firstName
        self.middleName = middleName
    }
    
    
}
